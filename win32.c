//          Copyright 2022, Vitali Baumtrok.
// Distributed under the Boost Software License, Version 1.0.
//     (See accompanying file LICENSE or copy at
//        http://www.boost.org/LICENSE_1_0.txt)

#if defined(VBSW_WIN32)

typedef unsigned long vbsw_ul_t;

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

typedef struct {
	WNDCLASSEX data;
	void *user_data;
	int (*reg)(void*);
	int (*unreg)(void*);
	int (*reged)(void*);
	void (*del)(void*);
	DWORD lasterr;
} vbsw_class_t;

typedef struct {
	vbsw_class_t *cls;
	HWND hndl;
	void *user_data;
	LRESULT (*procmsg)(HWND, UINT, WPARAM, LPARAM, void*, int*);
	int (*create)(void*,LPCTSTR,DWORD,int,int,int,int);
	void (*del)(void*);
	DWORD lasterr;
} vbsw_window_t;

static LRESULT CALLBACK windowProc(HWND const hWnd, const UINT message, const WPARAM wParam, const LPARAM lParam) {
	LRESULT result = 0;
	int processed = 0;
	if (message == WM_NCCREATE) {
		vbsw_window_t *const window = (vbsw_window_t*)(((CREATESTRUCT*)lParam)->lpCreateParams);
		if (window)
			SetWindowLongPtrW(hWnd, GWLP_USERDATA, (LONG_PTR)window);
	} else {
		vbsw_window_t *const window = (vbsw_window_t*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
		if (window)
			result = window->procmsg(hWnd, message, wParam, lParam, (void*)window, &processed);
	}
	if (processed)
		return result;
	return DefWindowProc(hWnd, message, wParam, lParam);
}

static int vbsw_class_register_struct(void *const cls) {
	vbsw_class_t *const clz = (vbsw_class_t*)cls;
	WNDCLASSEX wndCls;
	WNDCLASSEX *const cls_data = &((vbsw_class_t*)cls)->data;
	// class is not registered
	if (GetClassInfoEx(cls_data->hInstance, cls_data->lpszClassName, &wndCls) == 0) {
		const ATOM atom = RegisterClassEx(cls_data);
		if (atom != INVALID_ATOM)
			return 1;
		clz->lasterr = GetLastError();
		return 0;
	}
	return 1;
}

static int vbsw_class_unregister_struct(void *const cls) {
	vbsw_class_t *const clz = (vbsw_class_t*)cls;
	WNDCLASSEX wndCls;
	PWNDCLASSEX cls_data = (PWNDCLASSEX)(&((vbsw_class_t*)cls)->data);
	// class is registered
	if (GetClassInfoEx(cls_data->hInstance, cls_data->lpszClassName, &wndCls)) {
		if (UnregisterClass(clz->data.lpszClassName, clz->data.hInstance))
			return 1;
		clz->lasterr = GetLastError();
		return 0;
	}
	return 1;
}

static int vbsw_class_is_registered_struct(void *const cls) {
	vbsw_class_t *const clz = (vbsw_class_t*)cls;
	WNDCLASSEX wndCls;
	if (GetClassInfoEx(clz->data.hInstance, clz->data.lpszClassName, &wndCls) != 0)
		return 1;
	return 0;
}

static LRESULT vbsw_window_procmsg(HWND const hWnd, const UINT message, const WPARAM wParam, const LPARAM lParam, void *const wnd, int *const processed) {
	LRESULT result = 0;
	switch (message)
	{
	case WM_CLOSE:
		{
			vbsw_window_t *const window = (vbsw_window_t*)wnd;
			DestroyWindow(window->hndl);
			PostQuitMessage(0);
			*processed = 1;
		}
		break;
	}
	return result;
}

static int vbsw_window_create(void *const wnd, LPCTSTR const title, const DWORD style, const int x, const int y, const int w, const int h) {
	vbsw_window_t *const window = (vbsw_window_t*)wnd;
	window->hndl = CreateWindow(window->cls->data.lpszClassName, title, style, x, y, w, h, NULL, NULL, window->cls->data.hInstance, wnd);
	if (window->hndl == NULL) {
		window->lasterr = GetLastError();
		return 0;
	}
	return 1;
}

void vbsw_init(void **const instance, int *const cmd_show, int *const err, vbsw_ul_t *const err_win32) {
	HMODULE hModule = GetModuleHandle(NULL);
	if (hModule) {
		instance[0] = (void*)hModule;
		cmd_show[0] = SW_SHOWDEFAULT;
	} else {
		err[0] = 1;
		err_win32[0] = GetLastError();
	}
}

void vbsw_class_register(void *const cls, int *const err, vbsw_ul_t *const err_win32) {
	vbsw_class_t *const clz = (vbsw_class_t*)cls;
	if (clz->reg(cls) == 0) {
		err[0] = 2;
		err_win32[0] = (vbsw_ul_t)clz->lasterr;
	}
}

void vbsw_class_unregister(void *const cls, int *const err, vbsw_ul_t *const err_win32) {
	vbsw_class_t *const clz = (vbsw_class_t*)cls;
	if (clz->unreg(cls) == 0) {
		err[0] = 3;
		err_win32[0] = (vbsw_ul_t)clz->lasterr;
	}
}

void vbsw_class_is_registered(void *const cls, int *const result) {
	vbsw_class_t *const clz = (vbsw_class_t*)cls;
	result[0] = clz->reged(cls);
}

void vbsw_class_delete(void *const cls) {
	free(cls);
}

void vbsw_class_alloc(void **cls, void *const instance) {
	vbsw_class_t *const clz = (vbsw_class_t*)malloc(sizeof(vbsw_class_t));
	PWNDCLASSEX clz_data = &((vbsw_class_t*)clz)->data;
	clz_data->cbSize = sizeof(WNDCLASSEX);
	clz_data->style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	clz_data->lpfnWndProc = windowProc;
	clz_data->cbClsExtra = 0;
	clz_data->cbWndExtra = 0;
	clz_data->hInstance = (HINSTANCE)instance;
	clz_data->hIcon = NULL;
	clz_data->hCursor = NULL;
	clz_data->hbrBackground = NULL;
	clz_data->lpszMenuName = NULL;
	clz_data->lpszClassName = TEXT("vbsw_cls");
	clz_data->hIconSm = NULL;
	clz->user_data = NULL;
	clz->reg = vbsw_class_register_struct;
	clz->unreg = vbsw_class_unregister_struct;
	clz->reged = vbsw_class_is_registered_struct;
	clz->del = vbsw_class_delete;
	cls[0] = (void*)clz;
}

void vbsw_window_delete(void *const wnd) {
	free(wnd);
}

void vbsw_window_alloc(void **const wnd, void *const cls) {
	vbsw_window_t *const window = (vbsw_window_t*)malloc(sizeof(vbsw_window_t));
	window->cls = (vbsw_class_t*)cls;
	window->user_data = NULL;
	window->procmsg = vbsw_window_procmsg;
	window->create = vbsw_window_create;
	window->del = vbsw_window_delete;
}

#else

void vbsw_init(void **const instance, int *const cmdShow, int *const err) {}
void vbsw_class_register(void *const cls, int *const err, vbsw_ul_t *const err_win32) {}
void vbsw_class_unregister(void *const cls, int *const err, vbsw_ul_t *const err_win32) {}
void vbsw_class_is_registered(void *const cls, int *const result) {}
void vbsw_class_delete(void *const cls) {}
void vbsw_class_alloc(void **const cls, void *const instance) {}
void vbsw_window_delete(void *const wnd) {}
void vbsw_window_alloc(void **const wnd, void *const cls) {}

#endif /* #if defined(VBSW_WIN32) */
