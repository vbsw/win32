//          Copyright 2022, Vitali Baumtrok.
// Distributed under the Boost Software License, Version 1.0.
//     (See accompanying file LICENSE or copy at
//        http://www.boost.org/LICENSE_1_0.txt)

// Package win32 provides some wrapper functions for win32-api.
package win32

import "C"
import (
	"unsafe"
)

var (
	// Instance is intialized to HINSTANCE of this executable.
	Instance unsafe.Pointer
	// CmdShow is intialized to SW_SHOWDEFAULT
	CmdShow C.int
	// initialized marks the package as initialized.
	initialized bool
)

// Class holds a pointer to a C struct holding WNDCLASSEX. For more details see type vbsw_class_t in win32.c.
type Class struct {
	Ptr unsafe.Pointer
}

// Window holds a pointer to a C struct. For more details see type vbsw_window_t in win32.c.
type Window struct {
	Ptr unsafe.Pointer
}
