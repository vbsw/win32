//          Copyright 2022, Vitali Baumtrok.
// Distributed under the Boost Software License, Version 1.0.
//     (See accompanying file LICENSE or copy at
//        http://www.boost.org/LICENSE_1_0.txt)

// Package win32 provides some wrapper functions for win32-api.
package win32

// #cgo CFLAGS: -DVBSW_WIN32 -DUNICODE
// #cgo LDFLAGS: -luser32
// #include "win32.h"
import "C"
import (
	"errors"
	"unsafe"
)

// Init initializes the variables Instance and CmdShow.
func Init(err error) error {
	if err == nil && !initialized {
		var errC C.int
		var errWin32C C.vbsw_ul_t
		C.vbsw_init(&Instance, &CmdShow, &errC, &errWin32C)
		if errC == 0 {
			initialized = true
			return nil
		}
		return toError(int(errC), uint64(errWin32C))
	}
	return err
}

// NewClass returns a new instance of Class.
func NewClass() *Class {
	cls := new(Class)
	cls.init()
	return cls
}

// NewWindow returns a new instance of Window. Parameter can be nil.
func NewWindow(cls *Class) *Window {
	var clsPtr unsafe.Pointer
	wnd := new(Window)
	if cls != nil {
		clsPtr = cls.Ptr
	}
	wnd.init(clsPtr)
	return wnd
}

// Register registers the class. If class is already registered then nil is returned.
func (cls Class) Register() error {
	var errC C.int
	var errWin32C C.vbsw_ul_t
	C.vbsw_class_register(cls.Ptr, &errC, &errWin32C)
	if errC == 0 {
		return nil
	}
	return toError(int(errC), uint64(errWin32C))
}

// Unregister unregisters the class. If class is already unregistered then nil is returned.
func (cls *Class) Unregister() error {
	var errC C.int
	var errWin32C C.vbsw_ul_t
	C.vbsw_class_unregister(cls.Ptr, &errC, &errWin32C)
	if errC == 0 {
		return nil
	}
	return toError(int(errC), uint64(errWin32C))
}

// IsRegistered returns true, if class is registered.
func (cls *Class) IsRegistered() bool {
	var resultC C.int
	C.vbsw_class_is_registered(cls.Ptr, &resultC)
	return bool(resultC == 1)
}

// Delete releases memory allocated for the C struct. Ptr is set to nil.
func (cls *Class) Delete() {
	C.vbsw_class_delete(cls.Ptr)
	cls.Ptr = nil
}

// init allocates the C struct and initializes WNDCLASSEX.
func (cls *Class) init() {
	C.vbsw_class_alloc((*unsafe.Pointer)(&(cls.Ptr)), Instance)
}

// Delete releases memory allocated for the C struct. Ptr is set to nil.
func (wnd *Window) Delete() {
	C.vbsw_window_delete(unsafe.Pointer(wnd.Ptr))
	wnd.Ptr = nil
}

// init allocates the C struct.
func (wnd *Window) init(clsPtr unsafe.Pointer) {
	C.vbsw_window_alloc((*unsafe.Pointer)(&wnd.Ptr), clsPtr)
}

// toError converts an error number to a Go error object.
func toError(err int, errWin32 uint64) error {
	if err != 0 {
		var errStr string
		switch err {
		case 1:
			errStr = "get module instance failed"
		case 2:
			errStr = "register class failed"
		case 3:
			errStr = "unregister class failed"
		default:
			errStr = "win32 failed"
		}
		if errWin32 > 0 {
			errStr = errStr + " - " + toString(errWin32)
		}
		return errors.New(errStr)
	}
	return nil
}

// toString converts a positive integer value to string.
func toString(value uint64) string {
	var byteArr [20]byte
	var decimals int
	tenth := value / 10
	byteArr[19] = byte(value - tenth*10 + 48)
	value = tenth
	for decimals = 1; value > 0 && decimals < 20; decimals++ {
		tenth := value / 10
		byteArr[19-decimals] = byte(value - tenth*10 + 48)
		value = tenth
	}
	return string(byteArr[20-decimals:])
}
