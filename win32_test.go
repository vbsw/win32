//          Copyright 2022, Vitali Baumtrok.
// Distributed under the Boost Software License, Version 1.0.
//     (See accompanying file LICENSE or copy at
//        http://www.boost.org/LICENSE_1_0.txt)

package win32

import (
	"runtime"
	"testing"
)

func TestInit(t *testing.T) {
	err := Init(nil)
	if runtime.GOOS == "windows" {
		if err != nil {
			t.Error(err.Error())
		} else if !initialized {
			t.Error("initialized flag not set")
		} else if Instance == nil {
			t.Error("instance is nil")
		}
	} else {
		if err != nil {
			t.Error(err.Error())
		} else if initialized {
			t.Error("initialized flag is set, although not supported on " + runtime.GOOS)
		} else if Instance == nil {
			t.Error("instance is set, although not supported on " + runtime.GOOS)
		}
	}
}

func TestClassRegister(t *testing.T) {
	err := Init(nil)
	if err == nil {
		cls := NewClass()
		if runtime.GOOS == "windows" {
			if cls.Ptr == nil {
				t.Error("allocation of C struct class failed")
			} else {
				if cls.IsRegistered() {
					t.Error("IsRegistered failed, expected not registered")
				} else {
					err = cls.Register()
					if err != nil {
						t.Error(err.Error())
					} else if cls.IsRegistered() {
						err = cls.Unregister()
						if err != nil {
							t.Error(err.Error())
						}
					} else {
						t.Error("IsRegistered failed, expected registered")
					}
				}
				cls.Delete()
			}
		} else {
			if cls.Ptr != nil {
				t.Error("C memory allocated, although not supported on " + runtime.GOOS)
			}
		}
	} else if runtime.GOOS == "windows" {
		t.Error("register class failed to initialize module")
	}
}
