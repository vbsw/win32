#ifndef VBSWWIN32_H
#define VBSWWIN32_H

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned long vbsw_ul_t;
extern void vbsw_init(void **instance, int *cmd_show, int *err, vbsw_ul_t *err_win32);
extern void vbsw_class_alloc(void **cls, void *instance);
extern void vbsw_class_register(void *cls, int *err, vbsw_ul_t *err_win32);
extern void vbsw_class_unregister(void *cls, int *err, vbsw_ul_t *err_win32);
extern void vbsw_class_is_registered(void *cls, int *result);
extern void vbsw_class_delete(void *cls);
extern void vbsw_window_alloc(void **wnd, void *cls);
extern void vbsw_window_delete(void *wnd);

#ifdef __cplusplus
}
#endif

#endif /* VBSWWIN32_H */
