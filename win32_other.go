//          Copyright 2022, Vitali Baumtrok.
// Distributed under the Boost Software License, Version 1.0.
//     (See accompanying file LICENSE or copy at
//        http://www.boost.org/LICENSE_1_0.txt)

//go:build !windows

package win32

// Init initializes Instance and CmdShow.
func Init(err error) error {
	// not supported
	return err
}

// NewClass returns a new instance of Class.
func NewClass() *Class {
	cls := new(Class)
	// not supported
	return cls
}

// NewWindow returns a new instance of Window. Parameter can be nil.
func NewWindow(cls *Class) *Window {
	wnd := new(Window)
	// not supported
	return wnd
}

// Register registers the class. If class is already registered then nil is returned.
func (cls Class) Register() error {
	// not supported
	return nil
}

// Unregister unregisters the class. If class is already unregistered then nil is returned.
func (cls *Class) Unregister() error {
	// not supported
	return nil
}

// IsRegistered returns true, if class is registered.
func (cls *Class) IsRegistered() bool {
	// not supported
	return false
}

// Delete releases memory allocated for the C struct. Ptr is set to nil.
func (cls *Class) Delete() {
	// not supported
}

// init allocates the C struct and initializes WNDCLASSEX.
func (cls *Class) init() {
	// not supported
}

// Delete releases memory allocated for the C struct. Ptr is set to nil.
func (wnd *Window) Delete() {
	// not supported
}

// init allocates the C struct.
func (wnd *Window) init(clsPtr unsafe.Pointer) {
	// not supported
}
